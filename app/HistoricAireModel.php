<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class HistoricAireModel extends Model
{
    protected $table = 'historic_aire';     
    protected $fillable = [
        'temp', 'humedad',
    ];
}
