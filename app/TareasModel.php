<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class TareasModel extends Model
{
    public function empleado()
    {
        return $this->belongsTo(User::class);
    }

    public $timestamps = false;
    protected $table = 'tareas';     
    protected $fillable = [
        'nombre', 'sector', 'estado'
    ];
}
