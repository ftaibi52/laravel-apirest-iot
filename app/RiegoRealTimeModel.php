<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class RiegoRealTimeModel extends Model
{
    public $timestamps = true;
    protected $table = 'riego_realtime';     
    protected $fillable = [
        'estado' 
    ];
}