<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class Luz extends Model
{
    protected $table = 'luz';     
    protected $fillable = [
        'luz'
    ];
}
