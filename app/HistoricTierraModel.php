<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class HistoricTierraModel extends Model
{
    protected $table = 'historic_tierra';     
    protected $fillable = [
        'temp', 'humedad',
    ];
}
