<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class HistoricRiegoModel extends Model
{
    //
    
    protected $table = 'historic_riego';     
    protected $fillable = [
        'duracion', 'litros', 'inicio', 'fin',
    ];
}
