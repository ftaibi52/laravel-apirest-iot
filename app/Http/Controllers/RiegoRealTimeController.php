<?php

namespace dellIoT\Http\Controllers;

use dellIoT\RiegoRealTimeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RiegoRealTimeController extends Controller
{
    //

    //  select
    public function select()
    {

        return  RiegoRealTimeModel::all(); //ORM

    }
    public function select_last()
    {

        return  RiegoRealTimeModel::latest()->first(); //ORM

    }


    // update

    public function update(Request $request)
    {

        $RiegoCols = RiegoRealTimeModel::count();
        $tabla = 'riego_realtime';
        $columna = 'estado';

        if ($RiegoCols == 0) {
            DB::table($tabla)->insert([$columna => 0]);
            DB::table($tabla)->where('id', 1)->update([$columna => $request->estado]);
        } else {
            DB::table($tabla)->where('id', 1)->update([$columna => $request->estado]);
        }
    }
}
