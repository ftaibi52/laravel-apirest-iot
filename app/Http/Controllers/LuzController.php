<?php

namespace dellIoT\Http\Controllers;

use dellIoT\Luz;
use Illuminate\Http\Request;

class LuzController extends Controller
{
    public function select()
    {

        return  Luz::all(); //ORM

    }

    public function select_last()
    {

        return  Luz::latest()->first(); //ORM

    }

    public function insert(Request $request)
    {

        $luz = new Luz();
        $luz->luz = $request->luz;
        $luz->save();


        $data = array('Luz ' => $luz->luz);

        return $data;
    }
}
