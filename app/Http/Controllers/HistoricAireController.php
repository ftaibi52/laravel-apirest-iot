<?php

namespace dellIoT\Http\Controllers;

use dellIoT\HistoricAireModel;
use Illuminate\Http\Request;

class HistoricAireController extends Controller
{
    public function select()
    {

        return  HistoricAireModel::all(); //ORM

    }

    public function select_last()
    {

        return  HistoricAireModel::latest()->first(); //ORM

    }

    public function insert(Request $request)
    {
        $hisAire = new HistoricAireModel();
        $hisAire->temp = $request->temp;
        $hisAire->humedad = $request->humedad;
        $hisAire->save();

        $data = array('temp' => $hisAire->temp, 'humedad' => $hisAire->humedad);
        return $data;
    }
}
