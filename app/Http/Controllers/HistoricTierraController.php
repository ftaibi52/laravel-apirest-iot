<?php

namespace dellIoT\Http\Controllers;

use dellIoT\HistoricTierraModel;
use Illuminate\Http\Request;

class HistoricTierraController extends Controller
{

    public function select()
    {

        return  HistoricTierraModel::all(); //ORM

    }

    public function select_last()
    {

        return  HistoricTierraModel::latest()->first(); //ORM

    }

    public function insert(Request $request)
    {

        $hisTierra = new HistoricTierraModel();
        $hisTierra->temp = $request->temp;
        $hisTierra->humedad = $request->humedad;
        $hisTierra->save();


        $data = array('temp' => $hisTierra->temp, 'humedad' => $hisTierra->humedad);
        return $data;
    }
}
