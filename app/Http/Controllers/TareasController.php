<?php

namespace dellIoT\Http\Controllers;

use dellIoT\TareasModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TareasController extends Controller
{

    public function verTareas()
    {

        return  TareasModel::all(); //ORM

    }

    public function crearTarea(Request $request)
    {

        $tarea = new TareasModel();
        $tarea->nombre = $request->nombre;
        $tarea->sector = $request->sector;
        $tarea->estado = $request->estado;
        $tarea->user_id = $request->user_id;
        $tarea->save();

        $data = array('Nombre' => $tarea->nombre, 'Sector' => $tarea->sector, 'Usuario asignadod' => $tarea->user_id);
        return $data;
    }

    public function actualizarTarea(Request $request)
    {
        DB::table('tareas')->where('nombre', $request->nombre)->update(['estado' => $request->estado]);

        $data = array('Actualizada : Nombre' => $request->nombre, 'Estado' => $request->estado);
        return $data;
    }

    public function borrarTarea(Request $request)
    {
        DB::table('tareas')->where('nombre', $request->nombre)->delete();

        return 'Tarea borrada';
    }
}
