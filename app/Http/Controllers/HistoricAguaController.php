<?php

namespace dellIoT\Http\Controllers;

use dellIoT\HistoricAguaModel;
use Illuminate\Http\Request;

class HistoricAguaController extends Controller
{

    //  select
    public function select()
    {

        return  HistoricAguaModel::all(); //ORM

    }

    public function select_last()
    {

        return  HistoricAguaModel::latest()->first(); //ORM

    }

    // insert
    public function insert(Request $request)
    {
        $HisAgua = new HistoricAguaModel();
        $HisAgua->litros = $request->litros;
        $HisAgua->save();

        $data = array('litros' => $HisAgua->litros);
        return $data;
    }
}
