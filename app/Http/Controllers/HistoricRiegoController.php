<?php

namespace dellIoT\Http\Controllers;

use dellIoT\HistoricRiegoModel;
use Illuminate\Http\Request;

class HistoricRiegoController extends Controller
{
    //  select
    public function select()
    {

        return  HistoricRiegoModel::all(); //ORM

    }

    public function select_last()
    {

        return  HistoricRiegoModel::latest()->first(); //ORM

    }

    // insert
    public function insert(Request $request)
    {

        $HisRiego = new HistoricRiegoModel();
        $HisRiego->duracion = $request->duracion;
        $HisRiego->litros = $request->litros;
        $HisRiego->inicio = $request->inicio;
        $HisRiego->fin = $request->fin;
        $HisRiego->save();

        $data = array('duracion' => $HisRiego->duracion, 'litros' => $HisRiego->litros, 'inicio' => $HisRiego->inicio, 'fin' => $HisRiego->fin);
        return $data;
    }
}
