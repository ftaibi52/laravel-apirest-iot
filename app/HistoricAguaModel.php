<?php

namespace dellIoT;

use Illuminate\Database\Eloquent\Model;

class HistoricAguaModel extends Model
{
    //
    protected $table = 'historic_agua';     
    protected $fillable = [
        'litros'
    ];
}
