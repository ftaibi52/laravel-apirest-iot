<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            'name' => 'Paco',
            'email' => 'paco@gmail.com',
            'password' => '12345678'

        ]);
        DB::table('users')->insert([
            'name' => 'Pedro',
            'email' => 'pedro@gmail.com',
            'password' => '12345678'

        ]);
       
        DB::table('tareas')->insert([
            'nombre' => 'Desbrozar',
            'sector' => 1,
            'user_id' => 2

        ]);
        DB::table('tareas')->insert([
            'nombre' => 'Limpiar huerto',
            'sector' => 1,
            'user_id' => 1,

        ]);
        DB::table('tareas')->insert([
            'nombre' => 'Regar',
            'sector' => 1,
            'user_id' => 1,

        ]);
        DB::table('tareas')->insert([
            'nombre' => 'Barrer restos',
            'sector' => 1,
            'user_id' => 1,

        ]);
        DB::table('tareas')->insert([
            'nombre' => 'Podar',
            'sector' => 1,
            'user_id' => 1,

        ]);
    }
}
