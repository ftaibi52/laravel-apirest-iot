<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    // public routes
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    
    Route::post('details', 'UserController@details');
    
    Route::get('verDatosLuzUltimo', 'LuzController@select_last');
    Route::get('verDatosLuz', 'LuzController@select');
    Route::post('insertarDatosLuz', 'LuzController@insert');

    Route::get('verDatosTierraUltimo', 'HistoricTierraController@select_last');
    Route::get('verDatosTierra', 'HistoricTierraController@select');
    Route::post('insertarDatosTierra', 'HistoricTierraController@insert');

    Route::get('verDatosAmbienteUltimo', 'HistoricAireController@select_last');
    Route::get('verDatosAmbiente', 'HistoricAireController@select');
    Route::post('insertarDatosAmbiente', 'HistoricAireController@insert');

    Route::get('verNivelAguaUltimo', 'HistoricAguaController@select_last');
    Route::get('verNivelAgua', 'HistoricAguaController@select');
    Route::post('insertarNivelAgua', 'HistoricAguaController@insert');

    Route::get('verDatosRiegoUltimo', 'HistoricRiegoController@select_last');
    Route::get('verDatosRiego', 'HistoricRiegoController@select');
    Route::post('insertarDatosRiego', 'HistoricRiegoController@insert');

    Route::get('verEstadoRiegoUltimo', 'RiegoRealTimeController@select_last');
    Route::get('verEstadoRiego', 'RiegoRealTimeController@select');
    Route::post('actualizarEstadoRiego', 'RiegoRealTimeController@update');

    Route::get('tareasEmpleado', 'UserController@tareasEmpleado');

    Route::post('crearTarea', 'TareasController@crearTarea');
    Route::get('verTareas', 'TareasController@verTareas');
    Route::post('actualizarTarea', 'TareasController@actualizarTarea');
    Route::post('borrarTarea', 'TareasController@borrarTarea');

    // private routes
    
   // Route::middleware('auth:api')->group(function () {

    //});
    
});
